"""
Mortgage Backed Securities (MBS) Modeling by Jose V.Pagan and Jose A. Pagan
This program models the cash flows of certain Mortgage Backed Securities
inculding GNMAs and GNMA Serial pools at different prepayment speeds using 
the PSA and CPR prepayment models.  

The securities may be modeled as: 
originally issued,
at a particular "issue" date (when included as collateral in another MBS), or  
as updated based on the current pool factor (% of original pool outstanding). 

Future development will include pricing of the securities at different prepayment speeds
and discount yields, and being able to extract current pool factors from the Internet.
Other enhancements would include printing a price yield matrix, generating graphs, and
modeling lists of pools (as in the collateral of a MBS).
Eventually, we would like to add functionality to model mortgage backed securities,
including a proprietary scripting language.
"""

""" MortgagePool 
     
    All Mortgage Backed Securities (MBS) are derived from MortgagePool.
    
    MortgagePool includes basic information about a mortgage that is contained 
    in all classes

    :param issue_balance: current pool outstanding balance
    :param original_balance: original balance of mortgage pool
    :param run_mode: flag to detemine if balance is: 0==asIssued, 
            1==original_balance, 2==Updated 
    :param remaining_term: weighted average maturity of mortgage loan in months
    :param gross_coupon: weghted average coupon of mortgage loans in percent
    :param pass_through_rate: path through interestest rate in percent 
            (gross_coupon - servicingicing fee)
    :param price: purchase price of the mortgage pool
    :param original_term: original term of the mortgage pool in months
    :param wala: weighted average loan age
    :param pool_type: integer identifying type of mortgage pool (6 is GNMA)
    :param id_code: mortgage pool id number
    :param current_factor: fraction of pool outstading (current balance / 
            original balance)
    :param cusip: CUSIP number of pool - unique security identifier
    :param servicing_pct: servicingicing fee (usually 0.5 deducted from the 
            gross_coupon to pay for cost of servicingicing the loans)
    :param delay:
    :param speed: prepayment speed
    :param model: prepayment model (for speed) 0==PSA, 1==CPR
    :param legend: description of mortgage pool       

    Usage:         
        >>> MortgagePool.__init__(self,issue_balance,remaining_term,
        gross_coupon,pass_through_rate,price,original_term,pool_type,
        id_code,original_balance,currFactor,cusip,servicing_pct)
    
"""
class MortgagePool(object):
    def __init__(self, issue_balance, remaining_term, gross_coupon, 
            pass_through_rate, price, original_term, pool_type, id_code,
            original_balance, current_factor, cusip, servicing_pct):
        self.issue_balance = issue_balance            
        self.original_balance = original_balance              
        self.run_mode = 1                      
        self.remaining_term = remaining_term              
        self.gross_coupon = gross_coupon      
        self.pass_through_rate = pass_through_rate    
        self.price = price                  
        self.original_term = original_term            
        self.wala = original_term-remaining_term        
        self.pool_type = pool_type            
        self.id_code = id_code                
        
        self.current_factor = current_factor  
        self.cusip = cusip                  
        self.servicing_pct = servicing_pct              
       
        self.delay = None
        self.speed = 100
        self.model = 0 
        self.legend = None 

    """ make_payments calculates the monthly cashflows based on a given 
            prepayment speed and model. The securities may be modeled as
            originally issued, at a particular "issue" date (when included as 
            collateral in another MBS), or as updated based on the current pool
            factor (% of original pool outstanding).
        Usage: 
            >>> GP.make_payments(speed = 100,model = 0, run_mode = 1)  
    """
    def make_payments(self, speed, model,run_mode): 
        # Make payments based on given prepayment speed and prepayment model
        self.speed = speed
        self.model = model
        self.run_mode = run_mode
        self.init_payments()
        for month in range(1,self.remaining_term+1):
            self.amortize(month)
            
    """ init_payments is called by make_payments to initialize the lists where
        the monthly cashflows for the pool are stored, including scheduled 
        principal, unscheduled principal, interestest, servicingicing and 
        remaining pool (loan) balance. 
            :params sched_principal: 
            :params unsched_principal: 
            :params interest: 
            :params servicing: 
            :params bal: 
        
        Usage: 
            >>> make_payments.init_payments()
    """
    def init_payments(self):
        self.sched_principal = [0.0]
        self.unsched_principal = [0.0]
        self.interest = [0.0]
        self.servicing = [0.0]
        self.bal = [[self.issue_balance,self.original_balance,self.original_balance*self.current_factor][self.run_mode]] # poolBal == original_balance or issue_balance or original_balance*current_factor depending on whether runing asIssued or updated mode
    
    """ amortize is called by make_payments every month.  
        amortize uses the speed and model parameters to compute the % of loans 
        that prepay for the month in CPR (Conditional Payment Rate).
        
        :params interest:  is computed by applying the passthru rate to the
                loan balance of the previous month.  
        :params servicing: is couputed by applying the servicingicing rate to the 
                loan balance of the previous month.
        :params sched_principal: is calculated using the standard 
                Loan Payment Formula on the loan balance at the end of the 
                previous month.  The CPR is then used to calculate the 
                unsched_principal after deducting the sched_principal payment 
                from the loan balance at the end of the previous month.
        :params principal balance for the month: is calculated by substracting
                unscheduled and scheduled principal from the previous month 
                balance.
    """
    # Calculate prepayment speed in CPR
    def amortize(self,month):
        if self.model==1:  
            # using CPR prepayment model
            cpr =1.0 
        elif (self.wala+month)<=30:  
            #using PSA prepayment model 
            # (.002%*weighted average life for first 30 months)
            cpr = 0.002*(self.wala+month)  
        else:
            #using PSA prepayment model (6% CPR ofter 30 months)
            cpr = 0.06 
        # Calculate monthly servicingicing fee and interestest expense
        self.servicing.append(self.bal[-1]*self.servicing_pct/1200.0)
        self.interest.append(self.bal[-1]*(self.pass_through_rate/1200.0))
        # Calculate scheduled and unschenduled principal payments
        if month==self.remaining_term: # last period pay remaining balance
            self.sched_principal.append(self.bal[-1])
            self.unsched_principal.append(0)
        else:  # calculate scheduled and unscheduled principal payments for the month 
            self.sched_principal.append((self.gross_coupon/1200.0)*self.bal[-1]/((1+self.gross_coupon/1200.0)**(self.remaining_term-month+1)-1))
            self.unsched_principal.append((self.bal[-1]-self.sched_principal[-1])*(1.0-(1.0-cpr*self.speed/100.0)**(1.0/12)));
        self.bal.append(self.bal[-1]-self.unsched_principal[-1]-self.sched_principal[-1])

    """ print_cash_flow_report method prints the prepayment speed, the monthly cash flows (scheduled
        principal, unscheduled principal, interestest and servicing) and the balance at the end of the
        month of mortgage pool.  
        Usage: 
            >>> print_cash_flow_report()
    """
    def print_cash_flow_report(self):
        m = 0
        print "Prepayment Speed:"+str(self.speed),
        if self.model==1:
            print "CPR"
        else:
            print "PSA"    
        print "month\t  balance\tunsched_principal\tsched_principal\t interestest\t servicing"
        for n in self.bal:
            print "{:4d}\t{:12.2f}\t{:10.2f}\t{:10.2f}\t{:10.2f}\t{:10.2f}".format(m,self.bal[m],self.unsched_principal[m],self.sched_principal[m],self.interest[m], self.servicing[m] )
            m += 1
            
    def __str__(self):
        return self.legend       
 
                       
""" GNMAPool is derived from MortgagePool.
    Initializes a GNMA Mortgage Pool.    
    :params delay: GNMA I pools pay on the 15th of each month, 14 days after the record date (1st of each month)
    :params legend: print id_code
"""
class GNMAPool(MortgagePool):
    def __init__(self,issue_balance,remaining_term,gross_coupon,pass_through_rate,price,original_term,pool_type,id_code,original_balance,currFactor,cusip,servicing_pct):
        MortgagePool.__init__(self,issue_balance,remaining_term,gross_coupon,pass_through_rate,price,original_term,pool_type,id_code,original_balance,currFactor,cusip,servicing_pct)
        self.delay = 14 
        self.legend = "GNMA Pool #"+str(id_code)
        
""" GNMASerialPool is derived from GNMAPool.
    
    GNMA Serial Mortgage Pools usually consist of 100 units of $25,000 in principal each, 
    except for the last unit which pays $25,000 plus any remaining principal. 
    The last unit may be larger than $25,000 but under $50,000.  
    The units mature (are paid) sequentially in lots of $25,000, i.e., the entire principal of the unit is paid at once.
    Serial units pay interestest monthly and principal only at the maturity of the serial unit.
    
    <<<GNMASerialPool adds parameters to GNMAPool 
    including: first serial (owned), 
    last serial (owned), asumes serials in between are also included (owned),
    and total number of serials in the pool. 
    __init__ overrides the delay and legend params to describe GNMASerialPool.>>>
    
    :params first_serial: first unit (in list of consecutive units owned) of a GNMA Serial pool
    :params last_serial: last unit (in list of consecutive units owned) of a GNMA Serial pool
    :params total_serials: total number of serials in the original pool
    :params delay: GNMA Serial pools pay on the 25th of each month, 24 days after the record date (1st of each month)
    :params legend: print string with id_code first_serial, last_serial, total_serials
    
    Usage: 
        >>> GP = GNMASerialPool(issue_balance = 2508947,remaining_term = 237,gross_coupon = 7,pass_through_rate = 6.5,price = 102.4,original_term = 240, pool_type = 6,id_code = 377196, original_balance = 2508947,currFactor = 0.69110547,cusip = 'CUSIP',servicing_pct = 0.5,first_serial = 1,last_serial = 100,total_serials = 100)
"""
class GNMASerialPool(GNMAPool):
    def __init__(self,issue_balance,remaining_term,gross_coupon,pass_through_rate,price,original_term,pool_type,id_code,original_balance,currFactor,cusip,servicing_pct,first_serial,last_serial,total_serials):
        GNMAPool.__init__(self,issue_balance,remaining_term,gross_coupon,pass_through_rate,price,original_term,pool_type,id_code,original_balance,currFactor,cusip,servicing_pct) 
        self.first_serial = first_serial     
        self.last_serial = last_serial     
        self.total_serials = total_serials     
        self.delay = 24 
        self.legend = "GNMA Serial Pool #"+str(id_code)+" units "+ str(first_serial) + " to " + str(last_serial) + " out of " + str(self.total_serials)
        
    """ init_payments makes payments based on given prepayment speed and prepayment model.
        :param serialCashBuffer: 
        :param serialPrincipal:  to calculate serial payments and
        a list (serialBalance) where the monthly balance of the serial units is stored.
    """
    def init_payments(self): # 
        MortgagePool.init_payments(self)
        self.serialCashBuffer = 0.0 # idle cash remaining intra month for GNMA Serial pool (less than $25,000).
        self.serialPrincipal = 0.0 # the principal paid for the current month, in multiples of $25,000 (except for the last unit (unit 100) which can be larger).
        self.serialBalance = [self.calc_serial_balance()] # inserts the initial principal balance of the GNMA Serial units to the serial balance list.
        
        
    """ calc_serial_balance method computes initial principal balance of the GNMA Serial units selected (from first_serial thru last_serial).
    """  
    def calc_serial_balance(self):
        # Compute CalcBal
        UnitsRemaining = (int)(self.bal[0] / 25000)
        if (self.last_serial == self.total_serials):
            LastSerialRemainder = self.bal[0] - UnitsRemaining * 25000;
        else:
            LastSerialRemainder = 0;
        return (max(self.last_serial, self.total_serials - UnitsRemaining) - 
            max(self.first_serial - 1, self.total_serials - UnitsRemaining)) * 25000 + LastSerialRemainder # return calculated balance (calcBal) of Serial Pool ...
       
    """
        # The amortize method overrides the amortize method in class MortgagePool.  It first executes the
        # amortize method of MortgagePool and then uses additional logic to calculate the serial principal, 
        # interestest and servicingicing payments and the serial outstanding balance.  serialCashBuffer is used to
        # store idle cash remaining intra month (less than $25,000) to be paid in future months once
        # there is enough principal to pay a full $25,000 serial unit.  The last serial unit (unit 100) is 
        # paid when the principal balance of the pool reaches zero (usually in the last payment date).
    """
    def amortize(self, month):
        # Calculate monthly servicingicing fee and interestest expense
        MortgagePool.amortize(self,month)
        #{Calculation of serial principal payments}
        if month==self.remaining_term or abs(self.serialBalance[-1]-self.sched_principal[-1]-self.unsched_principal[-1]-self.serialCashBuffer)<0.001:
            self.serialPrincipal=self.serialBalance[-1]
        else:
            FUnit = self.total_serials+1-int((self.bal[month-1]+self.serialCashBuffer+0.001)/25000)
            LUnit = FUnit+int((self.sched_principal[-1]+self.unsched_principal[-1]+self.serialCashBuffer+0.001)/25000)-1
            if LUnit == self.total_serials:
                LUnit = LUnit-1
            self.serialPrincipal = 25000*max(0, min(LUnit, self.last_serial)-max(FUnit, self.first_serial)+1)
            
        self.serialCashBuffer = self.serialCashBuffer+self.sched_principal[-1]+self.unsched_principal[-1]-int((self.serialCashBuffer+self.sched_principal[-1]+self.unsched_principal[-1])/25000)*25000
        self.interest[-1] = self.pass_through_rate/1200*self.serialBalance[-1]
        self.servicing[-1] = (self.gross_coupon-self.pass_through_rate)/1200*self.serialBalance[-1]
        self.serialBalance.append(self.serialBalance[-1]-self.serialPrincipal)
        self.sched_principal[-1] = self.serialPrincipal    
        self.unsched_principal[-1] = 0 
        if month==self.remaining_term:
            self.bal=self.serialBalance

"""
calc_price is a function that calculates the price in % of a Mortgage Pool given a (discount)
rate, the level of precision (rounding) required, the number of days in the first period, and
the days of accrued interestest.  Precision can be 1/100000, 1/3200 or 1/6400, depending if rounding
is desired to the nearest 1/100th, 1/32nd or 1/64th of 1%, as is customary in the securities industry.
Days in first period is the number of days between the settlement date (day of sale) and the first payment date.
Days accrued is the number of days between the dated date (1st day of the month, when the underlying mortgage 
payments are due) and the settlement date (day of sale). 
"""             
def calc_price(pool,rate1,precision,days1stprd,daysaccrued):
    price=0;
    if rate1 >=0: 
        rate = ((rate1/200.0+1)**(1/6.0)-1)*1200 # calculates monthly interestest rate
        accruedint = daysaccrued/360.0* pool.gross_coupon/100* pool.bal[0] # calculates accrued interestest (interestest between that belongs to the previous owner and is deducted from the price)
        for n in range(1,len(pool.sched_principal)):
            price = price+(pool.sched_principal[n]+pool.unsched_principal[n]+pool.interest[n])/(1+rate/1200)**(days1stprd/30.0+n-1)
            price = price-accruedint
        if pool.bal[0] >0: #if tranche balance >0, ie not a residual
            calc_price = max(int(price / pool.bal[0] /precision)* precision,0.0)*100 #* pool.bal[0],0.0)
        else:
            calc_price = int(price*100+0.5)/100; #max(   ,0.0)
    else:
        calc_price = 0
    return calc_price   
    
            
def calc_yield(pool, proceeds1,days1stprd):
    TargetPrice =proceeds1
    ClosingDelay=0
    guess=0.07/12
    PmtDelay=days1stprd/30.0
    dval=1.0-PmtDelay
    counter=5
    calc_yield=-999
    while counter > 0:      
        if proceeds1< 0.0001: 
            break
        counter = counter -1
        OrDisc=1+guess
        DiscRate=OrDisc
        if guess <0:
            sign = -1.0
        else:
            sign = 1.0
        DelayYield=(abs(1.0+guess))**(PmtDelay-ClosingDelay)*sign
        Durtn=0
        TrialPrice=0
        # calculate present value of pool cash flows using guess discount rate to determine trial price
        for period in range (1,len(pool.bal)):
            if DiscRate<>0:
                CDisc=(pool.unsched_principal[period]+pool.sched_principal[period]+pool.interest[period])/DiscRate
            else:
                CDisc=0
            TrialPrice=TrialPrice+CDisc
            Durtn=Durtn-CDisc* period # calculates the duration of the cash flows
            DiscRate=DiscRate* OrDisc # calculates the discount rate for the next period
        # calculate difference between trial price and target price  
        fofy=(TrialPrice*(OrDisc/DelayYield))-TargetPrice
        # terminate if trial price is equal to the target price to the nearest 0.00005 precision.
        if(abs(fofy)<=0.00005):
            calc_yield=((1.0+guess)**6-1)*200.0
            break
        elif abs(fofy)>100000000:
            break
        fprimey=(Durtn/DelayYield)+TrialPrice*(dval/(DelayYield))
        if fprimey==0:
            break
        else:
            guess=guess-(fofy/fprimey)
    return calc_yield

 
def calc_duration(pool, yield1,days1stprd):
  if yield1==-999:
      calc_duration=-999
  else:
    tpv=0
    duration=0
    myield = ((yield1/200+1)**(1/6.0)-1)*1200
    for n in range (1, len(pool.bal)):
      pv = (pool.unsched_principal[n]+pool.sched_principal[n]+pool.interest[n])/(1+myield/1200)**(days1stprd/30.0+n-1)
      tpv = tpv+pv
      duration = duration+pv* (days1stprd/30.0+n-1)
    if tpv==0:
        calc_duration=-999
    else:
        calc_duration=duration / tpv / 12
    return calc_duration
             
def price_yield_matrix(pool,begYield,endYield,yieldInterval,prepaymentModel,begSpeed,endSpeed,speedInterval,precision,days1stprd,daysaccrued,run_mode):
    print "Price Yield Matrix"
    print "  Yield->",
    for rate in my_range(begYield,endYield,yieldInterval):
        print "{:8.2f}%".format(rate),
    print
    for speed in my_range(begSpeed,endSpeed,speedInterval):
        print "{:5.0f}".format(speed),
        if prepaymentModel==0:
            print "PSA",
        else:
            print "CPR",
        pool.make_payments(speed,prepaymentModel,run_mode)
        for rate in my_range(begYield,endYield,yieldInterval):
            print "{:9.2f}".format(calc_price(pool,rate,precision,days1stprd,daysaccrued)),
        print

def my_range(start, end, step):
    while start <= end:
        yield start
        start += step
                        

#GP = GNMAPool(issue_balance = 2508947,remaining_term = 237,gross_coupon = 7,pass_through_rate = 6.5,price = 102.4,original_term = 240,pool_type = 6,id_code = 377196,original_balance = 2508947,currFactor = 0.69110547,cusip = 'CUSIP',servicing_pct = 0.5)
#print GP
#GP.make_payments(100,1,1)
#GP.print_cash_flow_report()

GP = GNMASerialPool(issue_balance = 2508947,remaining_term = 237,gross_coupon = 7,pass_through_rate = 6.5,price = 102.4,original_term = 240, pool_type = 6,id_code = 377196, original_balance = 2508947,currFactor = 0.69110547,cusip = 'CUSIP',servicing_pct = 0.5,first_serial = 1,last_serial = 100,total_serials = 100)
print GP
GP.make_payments(speed = 100,model = 0, run_mode = 1)  
GP.print_cash_flow_report()
print
print "price is ", calc_price(pool=GP,rate1=6.5,precision=1.0/100000,days1stprd=30,daysaccrued=0)
print "yield is ", calc_yield(pool=GP,proceeds1=GP.bal[0],days1stprd=30), "%"
print "duration is ",calc_duration(pool=GP, yield1=7.0,days1stprd=30), "years"
print
price_yield_matrix(GP,begYield=5,endYield=7,yieldInterval=0.25,prepaymentModel=0,begSpeed=0,endSpeed=400,speedInterval=50,precision=1.0/100000,days1stprd=30,daysaccrued=0,run_mode=1)

               
""""
function calc_price(i1:integer; yield1,amount,precision:double):double;

Var
  n1:integer;
  yield, accruedint, price:double;

begin
  price:=0;
  if yield1>=0 then
  begin
    yield := (power(yield1/200+1,1/6.0)-1)*1200;
    accruedint := daysaccrued/360.0* td[i1].coupon/100* amount;
    for n1:=1 to MAXPERIODS do
    price := price+(tf[i1][n1].prin+tf[i1][n1].interest)/power(1+yield/1200, days1stprd/30.0+n1-1);
    price := price-accruedint;
    if td[i1].amt >0 then calc_price := max(int(price / amount /precision)* precision* amount,0.0)
    else calc_price := int(price*100+0.5)/100; //max(   ,0.0)
  end
  else calc_price := 0;
end;

function calc_yield(i1:integer; proceeds1:double):double;

Var
TrialPrice, OrDisc, DiscRate, CDisc, Durtn, PmtDelay, ClosingDelay,
  DelayYield, fofy, fprimey, dval, guess, TargetPrice: Double;
period, counter: integer;

begin
  TargetPrice :=proceeds1;
  ClosingDelay:=0;
  guess:=0.07/12;
  PmtDelay:=days1stprd/30;
  dval:=1-PmtDelay;
  counter:=50;
  calc_yield:=-999;
  while counter > 0 do
  begin
    if proceeds1< 0.0001 Then Break;
    counter := counter -1;
    OrDisc:=1+guess;
    DiscRate:=OrDisc;
    DelayYield:=my_power((1+guess),PmtDelay-ClosingDelay);
    Durtn:=0;
    TrialPrice:=0;
    for period:=1 to MAXPERIODS do
    begin
      if DiscRate<>0 then
      CDisc:=tf[i1][period].cash/DiscRate
      else CDisc:=0;
      TrialPrice:=TrialPrice+CDisc;
      Durtn:=Durtn-CDisc* period;
      DiscRate:=DiscRate* OrDisc;
    end;
    fofy:=(TrialPrice*(OrDisc/DelayYield))-TargetPrice;
    If(Abs(fofy)<=0.00005) Then
    begin
      calc_yield:=(power(1+guess,6)-1)*200;
      Break;
    end
    else if Abs(fofy)>100000000 then Break;
    fprimey:=(Durtn/DelayYield)+TrialPrice*(dval/(delayYield));
    if fprimey=0 then Break
    else guess:=guess-(fofy/fprimey);
  end;
end;

function calc_duration(i1:integer; yield1:double):double;

Var
n2: integer;
myield, pv, tpv, duration: double ;

begin
  if yield1=-999 then calc_duration:=-999
  else
  begin
    tpv:=0;
    duration:=0;
    myield := (power(yield1/200+1,1/6.0)-1)*1200;
    for n2:=1 to MAXPERIODS do
    begin
      pv := tf[i1, n2].cash/power(1+myield/1200, days1stprd/30.0+n2-1);
tpv := tpv+pv;
      duration := duration+pv* (days1stprd/30.0+n2-1);
    end;
    if tpv=0 then calc_duration:=-999
    else calc_duration:=duration / tpv / 12;
  end;
end;


Procedure CalcDates();
var
year, month, day: word;
begin
  with StructureForm do
  begin
    if UpdateComboBox.ItemIndex< 1 then
    begin
      daysaccrued:=int(StrtoDate(SettlementDate.text))-int(StrtoDate(DatedDate.text));
      days1stprd:=Days360(SettlementDate.Text, FirstPaymentDate.Text);
end
    else
    begin
      DecodeDate(StrToDate(FirstPaymentDateUpdate.Text)-StrtoFloat(DelayDays.Text),year,month,day);
      DatedDateUpdate.Text:=FormatDateTime('mm/dd/yyyy',
       EncodeDate(Year-1+(month+10)div 12,(Month+10)mod 12+1,1));
      daysaccrued:=int(StrtoDate(SettlementDateUpdate.text))-int(StrtoDate(DatedDateUpdate.text));
      days1stprd:=Days360(SettlementDateUpdate.Text, FirstPaymentDateUpdate.Text);
end;
  end;
end;

"""